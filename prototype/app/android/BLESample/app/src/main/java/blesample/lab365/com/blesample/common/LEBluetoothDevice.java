package blesample.lab365.com.blesample.common;

import android.bluetooth.BluetoothDevice;

/**
 * Created by ganesh on 4/27/16.
 */
public class LEBluetoothDevice {
    private BluetoothDevice btDevice;
    private int rssi = 0;

    public LEBluetoothDevice(BluetoothDevice device, int rssi) {
        this.btDevice = device;
        this.rssi = rssi;
    }

    public BluetoothDevice getBtDevice() {
        return btDevice;
    }

    public void setBtDevice(BluetoothDevice btDevice) {
        this.btDevice = btDevice;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }
}
