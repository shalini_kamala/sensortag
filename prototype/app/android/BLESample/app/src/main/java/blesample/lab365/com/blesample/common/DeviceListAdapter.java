package blesample.lab365.com.blesample.common;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import blesample.lab365.com.blesample.R;

/**
 * Created by shalini on 4/26/16.
 */
public class DeviceListAdapter extends BaseAdapter {
    private Activity activity;
    private List<LEBluetoothDevice> devices;
    private LayoutInflater mInflater;

    public DeviceListAdapter(Activity activity, List<LEBluetoothDevice> devices) {
        this.activity = activity;
        this.devices = devices;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mInflater == null)
            mInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.device_list_row, parent, false);

        TextView deviceNameTextView = (TextView)convertView.findViewById(R.id.device_name);
        TextView deviceAddressTextView = (TextView)convertView.findViewById(R.id.device_address);
        TextView deviceRssiTextView = (TextView)convertView.findViewById(R.id.device_rssi);
        LEBluetoothDevice device = devices.get(position);
        final String deviceName = device.getBtDevice().getName();
        if (deviceName != null && deviceName.length() > 0) {
            deviceNameTextView.setText(deviceName);
        }
        else {
            deviceNameTextView.setText(R.string.unknown_device);
        }
        deviceAddressTextView.setText(device.getBtDevice().getAddress());
        deviceRssiTextView.setText(device.getRssi() + Constants.SPACE + Constants.DBM);

        return convertView;
    }

    public void clear() {
        devices.clear();
    }

}
