package blesample.lab365.com.blesample.common;

/**
 * Created by shalini on 4/26/16.
 */
public class Constants {
    public static final int REQUEST_ENABLE_BT = 1;

    public static final String SPACE = " ";
    public static final String DBM = "dBm";
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String LIST_NAME = "NAME";
    public static final String LIST_UUID = "UUID";
}
